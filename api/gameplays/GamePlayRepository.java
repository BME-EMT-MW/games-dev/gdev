package hu.bme.emt.mw.gameplays;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GamePlayRepository extends PagingAndSortingRepository<GamePlay, String>, JpaSpecificationExecutor<GamePlay> {

}
