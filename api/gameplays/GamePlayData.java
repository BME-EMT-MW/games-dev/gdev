package hu.bme.emt.mw.gameplays;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GamePlayData {
    private String gameName;
    private String gameVersion;
    private String userAgent;
    private String userDevice; // Touchscreen, keyboard, etc.
    private String difficulty; // Easy/Medium/Hard
    private long score;
    private long displayedScore;
    private long startTimeEpoch;
    private long elapsedTimeMillis;
    private long pauseCount;
    private long pauseTimeSumMillis;
    private long errorCount;
    private String gameSpecificData;
}
