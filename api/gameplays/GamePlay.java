package hu.bme.emt.mw.gameplays;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GamePlay {
    @Id
    @Getter
    @Column(length = 42)
    private String uuid;
    @Getter
    private long playerId;
    @Getter
    @Embedded
    private GamePlayData data;
}
