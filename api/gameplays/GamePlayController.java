package hu.bme.emt.mw.gameplays;

import static hu.bme.emt.mw.users.UserController.HEADER_SESSION_TOKEN;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import hu.bme.emt.mw.users.User;
import hu.bme.emt.mw.users.UserController;
import io.swagger.annotations.ApiParam;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "/gameplays", produces = APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
public class GamePlayController {

    private static final Predicate<String> NOT_EMPTY = ((Predicate<String>) String::isEmpty).negate();

    private final UserController userController;
    private final GamePlayRepository gamePlayRepository;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<GamePlay> save(
            @RequestHeader(required = false, name = HEADER_SESSION_TOKEN) String token,
            @RequestBody GamePlayData data
    ) {
        Long playerId = Optional.ofNullable(token).filter(NOT_EMPTY)
                .map(userController::authenticateSession)
                .map(User::getId)
                .orElse(0L);
        String uuid = UUID.randomUUID().toString();
        GamePlay gamePlay = GamePlay.builder()
                .uuid(uuid)
                .playerId(playerId)
                .data(data)
                .build();
        final GamePlay savedGamePlay = gamePlayRepository.save(gamePlay);
        return status(CREATED).body(savedGamePlay);
    }

    @RequestMapping(method = GET)
    public ResponseEntity<Page<GamePlay>> list(Pageable p,
            @RequestParam(required = false) Long playerId,
            @RequestParam(required = false) String gameName,
            @RequestParam(required = false) @ApiParam(value = "Epoch millis", example = "0") Long startedAfter,
            @RequestParam(required = false) @ApiParam(value = "Epoch millis", example = "0") Long startedBefore
    ) {
        Specification<GamePlay> filter = (root, query, builder) -> builder.isTrue(builder.literal(true));
        if (playerId != null) filter = filter.and((it, q, is) -> is.equal(it.get("playerId"), playerId));
        if (gameName != null) filter = filter.and((it, q, is) -> is.equal(it.get("data").get("gameName"), gameName));
        if (startedAfter != null) filter = filter.and((it, q, is) -> is.greaterThanOrEqualTo(it.get("data").get("startTimeEpoch"), startedAfter));
        if (startedBefore != null) filter = filter.and((it, q, is) -> is.lessThanOrEqualTo(it.get("data").get("startTimeEpoch"), startedBefore));
        final Page<GamePlay> gamePlays = gamePlayRepository.findAll(filter, p);
        return status(OK).body(gamePlays);
    }

}
