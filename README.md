MW Repo for game developers
<br>Created 2019-09-12, hanak@emt.bme.hu
<br>Modified 2019-10-02, hanak@emt.bme.hu

<b>Setting up a demo:</b>

1. Clone this repo in the root of your webserver, or create a symlink in the
root.

2. Copy recursively gdev/api into the root, or create a symlink to api in the
root.

3. Start your webserver. You may create a virtual host.

4. Open the webpage in the browser: http://localhost/gdev, and open one of the
example games.

<b>Develop a new game:</b>

1. Install node in the gdev/games folder: open a terminal, and then 'npm install'.

   Install also other packages you will need:
     * npm install fs-extra
     * npm install command-line-args
     * npm install command-line-usage
     * npm install tsc
     * npm install typescript<p>

2. Create a new subfolder (eg. NEWGAME) in 'gdev/games/' for the new game. Copy
the subfolders and files of one of the example games into it.

3. Add the new game's url to the list in 'gdev/index.html'.

4. Write your game in Typesript (https://www.typescriptlang.org), handle sprites
with PixiJS (https://pixijs.download/dev/docs/index.html).

   You may develop your game with Webstorm, Emacs with TIDE, Visual Studio, etc.:
     * https://www.jetbrains.com/webstorm/
     * https://github.com/ananthakumaran/tide
     * https://code.visualstudio.com/docs/languages/typescript<p>

5. Translate and install your new game by: 
     * 'node bin/build -g NEWGAME' if the target is ES5 (ECMAScript/JavaScript 5);
     * 'node bin/build-es6 -g NEWGAME' if the target is ES6 (ECMAScript/JavaScript 6).

<p>More will be added later on Nodejs, Typescript, PixiJs, and the development
process.

<p>Good luck!
