class GameLog {
    constructor(gameName) {
        this.gameName = gameName;
        this.innerScore = null;
        this.displayedScore = null;
        this.startTime = null;
        this.difficulty = null;
        this.duration = null;
        this.pauseCtr = null;
        this.pauseDuration = null;
        this.gameVersion = "0.0";
        this.hardware = null;
        this.browser = null;
        this.stat = null;

        this.logs = [];
    }

    /**
     * Stores the eventlike logs about a game (e.g.: user clicked, mouse moved etc.)
     * @param log
     */
    add(log) {
        this.logs.push(log);
    }

    serialize() {
        return {
            difficulty: this.difficulty,
            displayedScore: this.displayedScore,
            elapsedTimeMillis: this.duration,
            errorCount: 0, // TODO wtf?
            gameName: this.gameName,
            gameSpecificData: JSON.stringify(this.stat),
            gameVersion: this.gameVersion,
            pauseCount: this.pauseCtr,
            pauseTimeSumMillis: this.pauseDuration,
            score: this.innerScore,
            startTimeEpoch: this.startTime,
            userAgent: this.browser,
            userDevice: this.hardware
        };
    }
}