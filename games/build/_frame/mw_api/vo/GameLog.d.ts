export interface GameLog
{
	gameName: string;       // the identifier of the game
	result: number;         // the number ScoreCalculator made after the game (200 - infinity)
	elapsedTime: number;    // time used by the user (in milliseconds)
	mixedData: any;         // can contain anything depending of the game
}