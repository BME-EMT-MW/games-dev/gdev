export interface GameSettings
{
	gameName: string;
	difficulty: number;
}