class FrameAPI {
	/**
	 * @param gameName {string}
	 */
	constructor(gameName) {
		this.gameName = gameName;
		this.timer = new Timer();

		// TODO MOCK -> switch to real server
		xhr("serverMock/getSettings.php", {gameName}, this.settingsArrived.bind(this));

		this.settings = {
			gameName: gameName,
			difficulty: 0
		}
	}

	settingsArrived(response) {
		this.settings.difficulty = parseInt(response) || 0;
	}

	onStart() {
		this.gameLog = new GameLog(this.gameName);
		this.timer.start();
		this.gameLog.add({"action": "start"});
		this.gameLog.startTime = this.timer.startTime;
		this.gameLog.difficulty = this.settings.difficulty;
		this.gameLog.hardware = getHardwareType();
		this.gameLog.browser = getBrowserType();
	}

	onPause() {
		this.timer.pause();
		this.gameLog.add({"action": "pause"});
	}

	onResume() {
		this.timer.resume();
		this.gameLog.add({"action": "resume"});
	}

	changeSettings(difficulty) {
		this.settings.difficulty = difficulty;
		xhr("serverMock/saveSettings.php", this.settings);
	}

	/**
	 * @param resultMessageCallback {function(message: string)}
	 */
	onStop(resultMessageCallback) {
		this.timer.stop();
		this.gameLog.add({"action": "stop"});
		this.gameLog.duration = this.timer.duration;
		this.gameLog.pauseCtr = this.timer.pauseCtr;
		this.gameLog.pauseDuration = this.timer.pauseDuration;

		var log = this.gameLog.serialize();
		xhr("/api/gameplays", JSON.stringify(log));
		if (log.displayedScore != null && log.score != null) { // game wasn't interrupted
			try {
				window.messageAPI.getMessage(log, resultMessageCallback);
			}
			catch (e) {
				console.warn("exception in message API: ", e.message);
			}
		}
	}

	onClose() {
		this.timer.stop();
		if (this.gameLog) this.gameLog.add({"action": "close"});
	}


}
