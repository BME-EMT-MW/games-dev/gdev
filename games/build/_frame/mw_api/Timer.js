class Timer {
	// date: Date;
	// startTime: number;
	// stopTime: number;
	// pauses: number[];
	// resumes: number[];
	// _state: TimerState;

	constructor() {
	}

	start() {
		if (this._state !== TimerState.running) {
			this._state = TimerState.running;
			this.startTime = Date.now();
			this.stopTime = null;
			this.pauses = [];
			this.resumes = [];
		}
	}

	/**
	 * Returns the current time in milliseconds
	 */
	get current() {
		return Date.now();
	}

	pause() {
		if (this._state !== TimerState.paused) {
			this._state = TimerState.paused;
			this.pauses.push(Date.now());
		}
	}

	resume() {
		if (this._state !== TimerState.running) {
			this._state = TimerState.running;
			this.resumes.push(Date.now());
		}
	}

	stop() {
		if (this._state !== TimerState.stopped) {
			this._state = TimerState.stopped;
			this.stopTime = Date.now();
		}
	}

	/**
	 * Returns time used without the paused sessions
	 */
	get duration() {
		const end = this.stopTime ? this.stopTime : Date.now();
		let duration = end - this.startTime;
		for (let i = 0; i < this.resumes.length; i++) {
			duration -= this.resumes[i] - this.pauses[i];
		}
		if (this.pauses.length > this.resumes.length) {
			// game currently paused -> remove time from last pause to end
			duration -= end - this.pauses[this.pauses.length - 1];
		}
		return duration;
	}

	/**
	 * Returns time used from start to stop (or to now if stop wasn't called yet)
	 */
	get fullDuration() {
		const end = this.stopTime ? this.stopTime : Date.now();
		return end - this.startTime;
	}

	isRunning() {
		return this._state === TimerState.running;
	}

	isPaused() {
		return this._state === TimerState.paused;
	}

	isStopped() {
		return this._state === TimerState.stopped;
	}

	/**
	 * Returns how many times pause() was called
	 */
	get pauseCtr() {
		return this.pauses.length;
	}

	/**
	 * Returns the time while Timer was in paused state
	 */
	get pauseDuration() {
		let duration = 0;
		for (let i = 0; i < this.resumes.length; i++) {
			duration += this.resumes[i] - this.pauses[i];
		}
		if (this.pauses.length > this.resumes.length) {
			const end = this.stopTime ? this.stopTime : Date.now();
			duration += end - this.pauses[this.pauses.length - 1];
		}
		return duration;
	}
}

const TimerState = {
	running: 0,
	paused: 1,
	stopped: 2
}
