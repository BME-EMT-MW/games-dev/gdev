
/**
 * @param url {string}
 * @param params json
 * @param [callback]
 */
function xhr(url, params, callback) {
	let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
	xmlhttp.open("POST", url);
	xmlhttp.setRequestHeader("Content-Type", "application/json");
	for (var header in window.top.mwApiHeaders) {
		if (window.top.mwApiHeaders.hasOwnProperty(header)) {
			xmlhttp.setRequestHeader(header, window.top.mwApiHeaders[header]);
		}
	}
	xmlhttp.send(params);
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState !== 4) return;
		if (xmlhttp.response != null && callback) {
			callback(xmlhttp.response);
		}
	};
}

// return values: desktop, mobile
function getHardwareType() {
	if (navigator && navigator.userAgent && navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
		return "mobile";
	}
	return "desktop";
}

function getBrowserType() {
	if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
	{
		return 'Opera';
	}
	else if(navigator.userAgent.indexOf("Chrome") != -1 )
	{
		return 'Chrome';
	}
	else if(navigator.userAgent.indexOf("Safari") != -1)
	{
		return 'Safari';
	}
	else if(navigator.userAgent.indexOf("Firefox") != -1 )
	{
		return 'Firefox';
	}
	else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
	{
		return 'IE';
	}
	else
	{
		return 'unknown';
	}
}