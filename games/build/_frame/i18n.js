class I18N {
    constructor(data) {
        if (typeof data === "string") {
            this.dictionary = JSON.parse(data);
        }
        else {
            this.dictionary = data;
        }
        window.__ = this.translate.bind(this);
    }

    translate(key) {
        if (this.dictionary[key]) {
            return this.dictionary[key];
        }
        return key;
    }

    addData(data) {
        try {
            var converted = typeof data === "string" ? JSON.parse(data) : data;
            Object.assign(this.dictionary, converted);
        }
        catch (e) {
            console.warn("error during parsing dictionary");
        }
    }
}
