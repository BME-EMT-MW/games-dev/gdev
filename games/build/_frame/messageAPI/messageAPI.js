var messageAPI = {};


function nextDifficulty(currentDifficulty, offset) {
    var nextDificulty = currentDifficulty + offset;
    switch (nextDificulty) {
        case 0: 
            return __('easy').toLowerCase();
        case 1:
            return __('medium').toLowerCase();
        case 2:
            return __('hard').toLowerCase();
    }

    console.log("Unable to calculate nextDifficulty: currentDifficulty="+currentDifficulty+"; offset="+offset);
    return __('current');
}

function  randomArrayElement(array) {
    if (array.length == 0) {
        return '';
    } else {
        return array[Math.floor(Math.random()*array.length)];
    }
}

(function(messageAPI) {
    messageAPI.getMessage = getMessage;

    /**
     * @param log - check the format of log in frame/mw_api/GameLog.js
     * @param resultMessageCallback {function(message: string): void}
     */
    function getMessage(log, resultMessageCallback) {
        var messageBases = [];
        var messageExtensions = [];
        var goodResultLimit = -1;




        switch (log.difficulty) {
            case 0:         // user played on easy
                goodResultLimit = 500;
                break;
            case 1:         // user played on medium
                goodResultLimit = 700;
                break;
            case 2:         // user played on hard
                goodResultLimit = 900;
                break;
        }

        if (log.score >= goodResultLimit) {
            messageBases.push(__('Good job'));
            messageBases.push(__('Excellent job'));
            messageBases.push(__('Perfect playing'));

            var nextDifficultyName = nextDifficulty(log.difficulty,1);
            if (nextDifficultyName != __('current')) {
                messageExtensions.push(", " + __('you should try the')+' '+nextDifficultyName+' '+__('level for more challenge')+"!");
                messageExtensions.push(", " + __('you could consider the')+' '+nextDifficultyName+' '+__('level for more challenge')+"!");
                messageExtensions.push(", " + __('you are master of the')+' '+nextDifficulty(log.difficulty,0)+' '+__('level')+"!");
            } else {
                messageExtensions.push(", " + __('you are master of this game')+"!");
            }
            
            
        } else if (log.score >= goodResultLimit/2) {
            messageBases.push(__('Nice job'));
            messageBases.push(__('Good playing'));

            var nextDifficultyName = nextDifficulty(log.difficulty,-1);
            var previousDifficultyName = nextDifficulty(log.difficulty,-1);

            if (nextDifficultyName != __('current')) {
                messageExtensions.push(", " + __('you should consider the')+' '+nextDifficultyName+' '+__('level for more challenge')+"!");
            } else if (previousDifficultyName != '') {
                messageExtensions.push(", " + __('you should consider the')+' '+previousDifficultyName+' '+__('level for higher scores')+"!");
            } 
        


            messageExtensions.push(", " + __('you should keep trying to get better')+"!");
            messageExtensions.push(", " + __('it will get better')+"!");
            messageExtensions.push(", " + __('more luck next time')+"!");

        } else {
            messageBases.push(__('Keep your head up'));
            messageBases.push(__('Do not give up'));
            messageBases.push(__('Nice try'));
            
            var previousDifficultyName = nextDifficulty(log.difficulty,-1);
            if (previousDifficultyName != '') {
                messageExtensions.push(", " + __('you should consider the')+' '+previousDifficultyName+' '+__('level for better results')+"!");
                messageExtensions.push(", " + __('what about switching to the')+' '+previousDifficultyName+' '+__('level for better results')+"?");
                messageExtensions.push(", " + __('how about trying the')+' '+previousDifficultyName+' '+__('level for better results')+"?");
            } else {
                messageExtensions.push(", " + __('you should keep trying to get better')+"!");
                messageExtensions.push(", " + __('an other game might fit you better')+"!");
                messageExtensions.push(", " + __('it will get better')+"!");
            }
            messageExtensions.push(", " + __('more luck next time')+"!");
        }

        message = randomArrayElement(messageBases) + randomArrayElement(messageExtensions);
        console.log("Result evaluation ang suggestions:")
        console.log("[Current level: "+log.difficulty+" = "+nextDifficulty(log.difficulty, 0)+"; Current score: "+log.score+"]");
        console.log("Bases: ");
        console.log(messageBases);
        console.log("Extensions: ");
        console.log(messageExtensions);
        resultMessageCallback(message);

        var flags = JSON.parse(window.localStorage.getItem("flags"));
        if (flags != null && flags.session != null) {
            var userId = flags.session.playerId;
    
            $.ajax(
                {
                    url: "https://frontvl.mit.bme.hu/api/gameplays?playerId="+userId, 
                    success: function(server_game_logs){
                        console.log("Previous game logs from the server:");
                        console.log(server_game_logs.content);
                }});
        }
    }

}(messageAPI));
