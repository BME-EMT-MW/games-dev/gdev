/**
 * @param alternativeForms {id: {title: string, src: string, className: string, callback: () => void}}
 * @param domElement {HTMLImageElement}
 * @param initFormId {string}
 * @constructor
 */
function FrameButton(alternativeForms, domElement, initFormId) {
    this.alternatives = alternativeForms;
    this.domElement = domElement;
    this.setTo(initFormId);
    this.activeFormId = initFormId;
    this.domElement.onclick = this.onClick.bind(this);
}

/**
 * @param alternativeId {string}
 */
FrameButton.prototype.setTo = function(alternativeId) {
    this.domElement.src = this.alternatives[alternativeId].src;
    this.domElement.className = this.alternatives[alternativeId].className;
    this.domElement.title = this.alternatives[alternativeId].title;
    this.activeFormId = alternativeId;
    this.enable();
};

FrameButton.prototype.enable = function() {
    this.domElement.classList.remove("disabled");
};

FrameButton.prototype.disable = function() {
    this.domElement.classList.add("disabled");
};

FrameButton.prototype.onClick = function () {
    if (!this.domElement.classList.contains("disabled")) {
        this.alternatives[this.activeFormId].callback();
    }
};
