(function () {
    "use strict";

    window.addEventListener("DOMContentLoaded", init);

    var game;
    var frameApi;
    var i18n;
    var mainButtonDom, exitButtonDom, settignsButtonDom, helpButtonDom;
    var container;

    var mainButton, exitButton, settingsButton, helpButton;

    function init() {
        i18n = new I18N({});

        container = document.getElementById("mw-game-content");
        mainButtonDom = document.getElementsByClassName("mw-game-button--start")[0];
        exitButtonDom = document.getElementsByClassName("mw-game-button--close")[0];
        settignsButtonDom = document.getElementsByClassName("mw-game-button--settings")[0];
        helpButtonDom = document.getElementsByClassName("mw-game-button--help")[0];

        settingsButton = new FrameButton({
            settings: {
                title: __("settings"),
                src: "icons/button-settings.png",
                className: "mw-game-button mw-game-button--settings",
                callback: onSettings
            }
        }, settignsButtonDom, "settings");

        helpButton = new FrameButton({
            help: {
                title: __("help"),
                src: "icons/button-help.png",
                className: "mw-game-button mw-game-button--help",
                callback: onHelp
            }
        }, helpButtonDom, "help");

        mainButton = new FrameButton({
            start: {
                title: __("start"),
                src: "icons/button-start.png",
                className: "mw-game-button mw-game-button--start",
                callback: onStart
            },
            pause: {
                title: __("pause"),
                src: "icons/button-pause.png",
                className: "mw-game-button mw-game-button--pause",
                callback: onPause
            },
            resume: {
                title: __("resume"),
                src: "icons/button-resume.png",
                className: "mw-game-button mw-game-button--resume",
                callback: onResume
            }
        }, mainButtonDom, "start");
        mainButton.disable(); // the game will call gameInitialized. From that moment playing is enabled

        exitButton = new FrameButton({
            close: {
                title: __("close"),
                src: "icons/button-close.png",
                className: "mw-game-button mw-game-button--close",
                callback: onClose
            },
            stop: {
                title: __("stop"),
                src: "icons/button-stop.png",
                className: "mw-game-button mw-game-button--stop",
                callback: onStop
            }
        }, exitButtonDom, "close");

        document.body.appendChild(container);

        window.addEventListener("hashchange", hashchanged);
        hashchanged();
    }

    function hashchanged() {
        var options = parseOptions(document.location.hash.substr(1));
        if (!options.gameName) {
            container.innerText = "Game name not specified!";
            return;
        }
        container.innerText = "";
        frameApi = new FrameAPI(options.gameName);

        i18n = new I18N({});
        xhr("GET", "../_i18n/" + options.gameName + "-" + options.lang + ".json", {then: languageInitialized});
        xhr("GET", "../_i18n/" + "frame-" + options.lang + ".json", {then: languageInitialized});

        game = {
            container: container,
            xhr_base: options.gameBase,
            onStop: gameFinished,
            gameInitialized: gameInitialized,
            api: frameApi
        };
        xhr("GET", game.xhr_base + "index.json", {then: onManifestLoaded});
    }

    function languageInitialized(lang_json) {
        i18n.addData(lang_json);
        mainButton.alternatives.start.title = __("start");
        mainButton.alternatives.pause.title = __("pause");
        mainButton.alternatives.resume.title = __("resume");
        exitButton.alternatives.close.title = __("close");
        exitButton.alternatives.stop.title = __("stop");
        settingsButton.alternatives.settings.title = __("settings");
        helpButton.alternatives.help.title = __("help");
    }

    /**
     * @return options
     * @return options.gameName
     * @return options.gameBase
     * @return options.lang
     */
    function parseOptions(query) {
        var i, l, key, val, part, parts = query.split("&"), options = {};
        for (i = 0, l = parts.length; i < l; i += 1) {
            part = parts[i].split("=");
            key = decodeURIComponent(part[0]);
            switch (part.length) { //@formatter:off
                case 1: val = true; break;
                case 2: val = decodeURIComponent(part[1]); break;
                default: console.warn("invalid option has too many parts: " + part); continue;
            } //@formatter:on
            options[key] = val;
            console.log(key + "=" + val);
        }
        return options;
    }

    function onManifestLoaded(content, status) {
        if (status !== 200) {
            console.error("Failed to load game manifest!");
            return;
        }
        var index = JSON.parse(content);
        window.mw_init = null;
        addStyleSheets(index.stylesheets);
        executeScripts(index.scripts);
    }

    function addStyleSheets(sheets) {
        if (!sheets || !sheets.length) return;
        var i, l, e;
        for (i = 0, l = sheets.length; i < l; i += 1) {
            e = document.createElement("link");
            e.rel = "stylesheet";
            e.type = "text/css";
            e.className = "mw-game-style mw-game-style--" + name;
            e.href = game.xhr_base + sheets[i];
            document.head.appendChild(e);
        }
    }

    function executeScripts(scripts) {
        if (!scripts || !scripts.length) return;
        // The recursive approach is necessary because script execution is asynchronous.
        executeScript(0);
        function executeScript(i) {
            if (i >= scripts.length) {
                onScriptsExecuted();
                return;
            }

            var e = document.createElement("script");
            e.src = game.xhr_base + scripts[i];
            e.addEventListener("load", next);
            document.head.appendChild(e);

            function next() {
                executeScript(i + 1);
            }
        }
    }

    function onScriptsExecuted() {
        // Call the mw_init function, if it was defined by the executed scripts.
        if (typeof window.mw_init === "function") {
            window.mw_init.call(game, game);
        } else {
            console.warn("Game initializer not found: 'window.mw_init' is not a function!");
        }
    }

    function forward(action) {
        if (typeof game[action] === "function") game[action]();
        else console.warn("The game has no '" + action + "' action!");
    }

    function onSettings() {
        forward("configure");
    }

    function onHelp() {
        forward("help");
    }

    function onStart() {
        settingsButton.disable();
        helpButton.disable();
        mainButton.setTo("pause");
        exitButton.setTo("stop");
        frameApi.onStart();
        forward("start");
    }

    function onPause() {
        helpButton.enable();
        mainButton.setTo("resume");
        frameApi.onPause();
        forward("pause");
    }

    function onResume() {
        helpButton.disable();
        mainButton.setTo("pause");
        frameApi.onResume();
        forward("resume");
    }

    function onStop() {
        forward("stop");
        settingsButton.enable();
        helpButton.enable();
        mainButton.setTo("start");
        exitButton.setTo("close");
        frameApi.onStop();
    }

    function onClose() {
        frameApi.onClose();
        if (window !== window.top) {
            window.top.location.hash = "/games";
        }
    }

    function gameInitialized() {
        mainButton.enable();
    }

    function gameFinished(resultMessageCallback) {
        settingsButton.enable();
        helpButton.enable();
        mainButton.setTo("start");
        exitButton.setTo("close");
        frameApi.onStop(resultMessageCallback);
    }

    // xhr is a wrapper over XmlHttpRequest.
    function xhr(method, url, x) {
        var req, header, options = Object(x);

        req = new XMLHttpRequest(); // @formatter:off
        req.withCredentials    = options.withCredentials || false;
        req.responseType       = options.responseType    || "";
        req.timeout            = options.timeout         || 0;
        req.self               = options.self;
        req.then               = options.then;
        req.onreadystatechange = xhr_onRSC; // @formatter:on

        req.open(method, url, true);

        if (options.headers) for (header in options.headers) {
            req.setRequestHeader(header, options.headers[header]);
        }

        req.send(options.data);
    }

    // xhr_onRSC runs when the XMLHttpRequest changes state.
    function xhr_onRSC() {
        if (this.readyState !== 4) return;
        if (typeof this.then === "function") {
            this.then.call(this.self || this, this.responseText, this.status, this);
        }
    }

})();
