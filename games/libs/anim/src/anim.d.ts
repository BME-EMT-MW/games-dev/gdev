/// <reference path="../../pixi.js.d.ts" />
declare module anim {
    class Scheduler {
        private runningProcesses;
        private intervals;
        constructor();
        run(process: Process, sprite: PIXI.Sprite, endCallback?: () => void, callbackContext?: any): void;
        pause(): void;
        resume(): void;
        stop(): void;
        addRunningProcess(process: Process): void;
        removeRunningProcess(process: Process): void;
        setTimeOut(duration: number, callback: () => void, context?: any): Delay;
        clearTimeOut(delay: Delay): void;
        setInterval(duration: number, callback: () => void, context?: any): Process;
        private nextInterval();
        clearInterval(delay: Process): void;
    }
    var scheduler: Scheduler;
    var Types: {
        Process: string;
        Delay: string;
        Sequence: string;
        Parallel: string;
        FadeTo: string;
        MoveTo: string;
        MoveBy: string;
        RotateTo: string;
        RotateBy: string;
        ScaleTo: string;
        ScaleBy: string;
    };
    class Process {
        id: number;
        protected type: string;
        protected duration: number;
        protected currTime: number;
        protected target: PIXI.Sprite;
        protected endCallback: () => void;
        protected endCallbackContext: any;
        isStopped: boolean;
        private prevTimestamp;
        private currRequestId;
        constructor();
        /**
         * @param {number} duration of animation in milliseconds
         * @returns {bc.Process}
         */
        setDuration(duration: number): Process;
        setTarget(target: PIXI.Sprite): void;
        setEndCallback(callback: () => void, callbackContext?: any): void;
        run(): void;
        protected requestAnimCallback: () => void;
        pause(): void;
        resume(): void;
        stop(): void;
        protected update(delta: number): void;
    }
    class Delay extends Process {
        constructor();
        protected update(delta: any): void;
    }
    class Sequence extends Process {
        private subProcesses;
        private currentRunningIndex;
        constructor(processes: Process[]);
        run(): void;
        private increaseCounter;
        private next();
    }
    class Parallel extends Process {
        private subProcesses;
        private endedProcesses;
        constructor(processes: Process[]);
        run(): void;
        private checkEnd;
    }
    class FadeTo extends Process {
        private to;
        private fadeDiffPerMilliSec;
        constructor(to: number);
        run(): void;
        protected update(delta: number): void;
    }
    class MoveTo extends Process {
        private to;
        private dir;
        constructor(to: PIXI.PointLike);
        run(): void;
        protected update(delta: number): void;
    }
    class MoveBy extends Process {
        private dir;
        private by;
        constructor(by: PIXI.PointLike);
        run(): void;
        protected update(delta: number): void;
    }
    class ScaleTo extends Process {
        private to;
        private amount;
        constructor(x: number, y?: number);
        run(): void;
        protected update(delta: number): void;
    }
    class ScaleBy extends Process {
        private amount;
        private by;
        constructor(by: PIXI.PointLike);
        run(): void;
        protected update(delta: number): void;
    }
}
