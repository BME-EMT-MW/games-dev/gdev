declare module anim {
	export class Scheduler {
		private runningProcesses: Process[];

		public run(process: Process, sprite: PIXI.Container, endCallback?: (target: PIXI.Container) => void, callbackContext?: any): void;
		public pause(): void;
		public resume(): void;
		public stop(): void;
		public addRunningProcess(process: Process);
		public removeRunningProcess(process: Process);
		public setTimeOut(duration: number, callback: () => void, context?: any): Delay;
		public clearTimeOut(delay: Delay): void;
		public setInterval(duration: number, callback: () => void, context?: any): Process;
		public clearInterval(delay: Process): void;
	}

	export var scheduler: Scheduler;
	export var Types: ProcessTypes;
	export interface ProcessTypes {
		Process: string,
		Delay: string,
		Sequence: string,
		Parallel: string,
		FadeTo: string,
		MoveTo: string,
		MoveBy: string,
		RotateTo: string,
		RotateBy: string,
		ScaleTo: string,
		ScaleBy: string
	}

	export class Process {
		public id: number;
		protected type: string;
		protected duration: number;
		protected currTime: number;
		protected target: PIXI.Sprite;
		protected endCallback: (target: PIXI.Container) => void;
		protected endCallbackContext: any;
		private prevTimestamp: number;
		private currRequestId: number;

		/**
		 * @param {number} duration of animation in milliseconds
		 * @returns {bc.Process}
		 */
		public setDuration(duration: number): Process;
		public setTarget(target: PIXI.Sprite): void;
		public setEndCallback(callback: (target: PIXI.Container) => void, callbackContext?: any): void;
		public run(): void;
		protected requestAnimCallback: void;
		public pause(): void;
		public resume(): void;
		public stop(): void;
		protected update(delta: number): void;
	}

	export class Delay extends Process {}
	export class Sequence extends Process {
		constructor(processes: Process[]);
	}

	export class Parallel extends Process {
		constructor(processes: Process[]);
	}

	export class FadeTo extends Process {
		constructor(to: number);
	}

	export class MoveTo extends Process {
		constructor(to: PIXI.PointLike);
	}

	export class MoveBy extends Process {
		constructor(by: PIXI.PointLike);
	}

	export class ScaleTo extends Process {
		constructor(x: number, y?: number);
	}

	export class ScaleBy extends Process {
		constructor(by: PIXI.PointLike);
	}

	export class RotateTo extends Process {
		constructor(angle: number);
	}

	export class RotateBy extends Process {
		constructor(angle: number);
	}
}