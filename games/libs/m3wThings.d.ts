//the i18n function
declare function __(text: string) : string;

declare class InitParameter {
    container: HTMLElement;
    xhr_base: string;
    onStop: (resultMessageCallback: (message: string) => void) => void;
    gameInitialized: () => void;
    api: FrameApi;
}

declare class FrameApi {
    public gameName: string;
    public timer: Timer;
    public settings: {gameName: string, difficulty: number};
    public gameLog: GameLog;
    public changeSettings: (difficulty: number) => void;
}

declare class Timer {
    public duration: number;
    public isRunning: boolean;
    public isPaused: boolean;
    public isStopped: boolean;
}

declare class GameLog {
    public innerScore: number;
    public displayedScore: number;
    public difficulty: number;
    public gameVersion: string;
    public stat: any;
    public add(log: any): void;
}
