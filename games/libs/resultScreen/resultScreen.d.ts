declare class ResultScreen extends PIXI.Container {
	constructor(screenWidth: number, screenHeight: number);
	public setScore(score: number): void;
	public messageCallback(message: string): void;
	public setReplayCallback(replayCallback: () => void, ctx?: any): void;
}
