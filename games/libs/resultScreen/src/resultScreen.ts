/// <reference path="../../pixi.js.d.ts"/>
/// <reference path="../../anim/anim.d.ts"/>
/// <reference path="../../m3wThings.d.ts"/>

class ResultScreen extends PIXI.Container {
	private yourResultIs: PIXI.Text;
	private scoreLabel: PIXI.Text;
	private score: number;
	private currScore: number;
	private intervalId: anim.Process;
	private resultMessage: PIXI.Text;
	private gameEnded: PIXI.Text;
	private button: Button;

	constructor(screenWidth: number, screenHeight: number) {
		super();

		this.score = 0;

		let addition = 100;
		if (screenWidth <= 700) addition = 100;

		const graphics = new PIXI.Graphics();
		let baseW = screenWidth / 2 + addition;
		let baseH = screenWidth / 2 / 1.6667 + addition / 1.6667;

		this.width = baseW;
		this.height = baseH;
		this.position.set(
			screenWidth / 4 - addition / 2,
			screenHeight / 2 - screenWidth / 2 / 1.6667 / 2 - addition / 2
		);

		// shadow
		graphics.beginFill(0x402b21, 0.5);
		graphics.drawRoundedRect(5, 5, baseW, baseH, 5);
		// border
		graphics.beginFill(0x0);
		graphics.drawRoundedRect(-1, -1, baseW + 2, baseH + 2, 5);
		// white background
		graphics.beginFill(0xffffff);
		graphics.drawRoundedRect(0, 0, baseW, baseH, 5);
		graphics.endFill();
		this.addChild(graphics);

		this.gameEnded = new PIXI.Text(__("game ended"), {fontSize: 14 * screenWidth / 800});
		this.gameEnded.anchor.set(0.5, 0.5);
		this.gameEnded.position.set(baseW / 2, baseH / 9);
		this.addChild(this.gameEnded);

		this.yourResultIs = new PIXI.Text(__("your result is:"), {fontSize: 26 * screenWidth / 800, fontWeight: "bold"});
		this.yourResultIs.anchor.set(0.5, 0.5);
		this.yourResultIs.position.set(baseW / 2, baseH / 3.5);
		graphics.addChild(this.yourResultIs);

		this.scoreLabel = new PIXI.Text("0", {fontSize: 26 * screenWidth / 800, fontWeight: "bold"});
		this.scoreLabel.anchor.set(0.5, 0.5);
		this.scoreLabel.position.set(baseW / 2, baseH / 2.4);
		this.addChild(this.scoreLabel);

		this.resultMessage = new PIXI.Text("", {
			fontSize: 14 * screenWidth / 800,
			breakWords: true,
			wordWrap: true,
			wordWrapWidth: screenWidth / 2
		});
		this.resultMessage.anchor.set(0.5, 0);
		this.resultMessage.position.set(baseW / 2, baseH / 2);
		this.addChild(this.resultMessage);

		// play button
		baseW = baseW * 0.6;
		baseH = baseH / 6;
		this.button = new Button(baseW, baseH, __("play again"), 22 * screenWidth / 800);
		this.button.position.set(this.width / 2 - baseW / 2, this.height / 1.45);
		this.button.on("click", () => {
			(<HTMLElement> document.getElementsByClassName("mw-game-button--start")[0]).click();
		});
		this.addChild(this.button);
	}

	public setScore(score: number) {
		this.score = score;
		this.currScore = 0;
		this.intervalId = anim.scheduler.setInterval(20, this.increase, this);
	}

	private increase() {
		this.currScore += 15;
		if (this.currScore >= this.score) {
			this.currScore = this.score;
			anim.scheduler.clearInterval(this.intervalId);
		}
		this.scoreLabel.text = this.currScore.toString();
	}

	public messageCallback = (message: string) => {
		this.resultMessage.text = message;
	}

	public setReplayCallback(replayCallback: () => void, ctx?: any): void {

	}
}

class Button extends PIXI.Container {
	private graphics: PIXI.Graphics;
	private w: number;
	private h: number;

	constructor(w: number, h: number, text: string, fontSize: number)
	{
		super();
		this.width = this.w = w;
		this.height = this.h = h;

		this.graphics = new PIXI.Graphics();
		this.addChild(this.graphics);
		this.redraw(false);

		const label = new PIXI.Text(text, {fontSize: fontSize, fontWeight: "bold"});
		label.anchor.set(0.5, 0.5);
		label.position.set(w / 2, h / 2);
		this.addChild(label);

		this.interactive = true;
		this.on("mouseover", () => this.redraw(true), this);
		this.on("mouseout", () => this.redraw(false), this);
	}

	private redraw(highLighted: boolean) {
		this.graphics.beginFill(0x0);
		this.graphics.drawRoundedRect(0, 0, this.w, this.h, 5);
		this.graphics.beginFill(highLighted ? 0xd1e7f2 : 0xc1d7e2);
		this.graphics.drawRoundedRect(1, 1, this.w - 2, this.h - 2, 5);
		this.graphics.endFill();
	}
}
