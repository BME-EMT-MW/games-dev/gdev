/// <reference path="../../../libs/resultScreen/resultScreen.d.ts"/>

module hello {
	export var version = "1.0"; // a játék verzió száma
	export var log: GameLog = null; // globális referencia a gamelog objektumra (ebbe kell gyűjtened majd a játékosról adatokat)
	export var res: {  // a játékban használt képek elérési útvonala lesz benne
		bgForEmpty: string,
	};

	export var initParams: InitParameter; // a játékfuttató keretrendszer nyújtotta szolgáltatások érhetők itt el
	export var width: number = 1000; // a VIRTUÁLIS játéktér mérete (a valóságban a canvas ki fogja tölteni a rendelkezésre álló teret szóval ez nem a tényleges képernyő méret)
	export var height: number = 1000; // amit viszont meghatároznak az a játék méretaránya (ez itt pl 1:1 arányú játék)
	export var config: {    // interface a config.json tartalmához
		"wrongButtonPush": {
			"good": number,
			"acceptable": number,
			"weight": number
		},
		"playTime": {
			"good": number,
			"acceptable": number,
			"weight": number
		},
	}[];

	export var views: { // a játék különböző nézetei
		settings: Settings; // ezt látod, amikor megnyomod a settingset
		help: Help; // -||- helpet
		result: ResultScreen; // ez a popup a játék végén
		empty: Empty; // ezt látod amikor megnyitod a játékot de még nem indítod el
	}

	export var game: Blocks; // globál referencia a játékod példányára
}

function mw_init(initParams: InitParameter)
{
	let xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET", this.xhr_base + "config.json");
	xmlhttp.send();
	xmlhttp.onreadystatechange = () => { // config.json letöltése
		if (xmlhttp.readyState !== 4) return;
		hello.config = JSON.parse(xmlhttp.response);
	};

	hello.res = { // kép url-k inicializálása
		bgForEmpty: this.xhr_base + "res/img/splash.png",
	};

	PIXI.loader.add(hello.res.bgForEmpty).load(() => { // képek letöltése
		hello.initParams = initParams;

		hello.views = { // nézetek inicializálása
			settings: new hello.Settings(),
			help: new hello.Help(),
			result: new ResultScreen(hello.width, hello.height),
			empty: new hello.Empty()
		};
		hello.game = new hello.Blocks(this.container); // a játék inicializálása

		// játékkezelő gombokhoz be kell kötni a játék egy-egy callback metódusát
		this.start = hello.game.newGame.bind(hello.game);
		this.pause = hello.game.pause.bind(hello.game);
		this.resume = hello.game.resume.bind(hello.game);
		this.stop = hello.game.stop.bind(hello.game);
		this.close = hello.game.exit.bind(hello.game);
		this.configure = hello.game.showSettings.bind(hello.game);
		this.help = hello.game.showHelp.bind(hello.game);
		initParams.gameInitialized(); // jelezzük a játékfuttató keretrendszernek, hogy kész vagyunk a játék inicializálásával
	});
}
