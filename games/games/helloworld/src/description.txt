"Blocks is a logic puzzle; its goal is to eliminate all blocks from the play area."

"You may swap neighbouring blocks, or drag a block to a neighbouring empty space."

"Three or more neighbouring blocks of the same colour in a row or a column are called a group."
"Groups are short-lived: when a group is formed it disappears immediately."

"Less than three remaining blocks of the same colour are called orphans."
"Orphans are immortal: they cannot be eliminated."

"Due to gravity, blocks above vacated places fall down."

"A move may yield the formation of more than one groups disappearing at the same time."
"Take care to avoid the creation of orphans."
