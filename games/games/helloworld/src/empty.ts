
module hello {
	export class Empty extends PIXI.Sprite {
		private title: PIXI.Text;

		constructor() {
			super(PIXI.loader.resources[res.bgForEmpty].texture);
			this.anchor.set(0.5, 0.5);
			const size = (width < height) ? width : height;
			this.width = size;
			this.height = size;
			this.alpha = 0.8;
			this.position.set(width / 2, height / 2);

			this.title = new PIXI.Text(__("hello world"), {align: 'center', fontSize: 70});
			this.title.anchor.set(0.5, 0.5);
			this.addChild(this.title);
		}


	}
}
