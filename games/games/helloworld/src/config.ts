// referenciák deklarációs fájlokra, amik az IDE-det segítik (ami meg ebből fakadóan téged)

/// <reference path="../../../libs/m3wThings.d.ts"/>
/// <reference path="../../../libs/pixi.js.d.ts"/>
/// <reference path="../../../libs/anim/anim.d.ts"/>

// meg kell adnod az összes fájlod elérési útját itt, hogy a fordítás működjön

/// <reference path="./empty.ts"/>
/// <reference path="./settings.ts"/>
/// <reference path="./help.ts"/>
/// <reference path="./map.ts"/>
/// <reference path="./helloworld.ts"/>
/// <reference path="./helloworld-init.ts"/>
