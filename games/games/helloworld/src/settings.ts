
module hello {
	export class Settings extends PIXI.Container {
		private difficulty: PIXI.Text;
		private easy: PIXI.Text;
		private medium: PIXI.Text;
		private hard: PIXI.Text;
		private enlargeEasy: anim.Process;
		private minifyEasy: anim.Process;
		private enlargeMedium: anim.Process;
		private minifyMedium: anim.Process;
		private enlargeHard: anim.Process;
		private minifyHard: anim.Process;
		private switch: PIXI.Text;
		private background: PIXI.Sprite;

		constructor() {
			super();
			this.background = new PIXI.Sprite(PIXI.loader.resources[res.bgForEmpty].texture);
			this.background.anchor.set(0.5, 0.5);
			const size = (width < height) ? width : height;
			this.background.width = size;
			this.background.height = size;
			this.background.alpha = 0.8;
			this.addChild(this.background);
			this.position.set(width / 2, height / 2);

			this.difficulty = new PIXI.Text(__("difficulty"), {align: 'center', fontSize: 60});
			this.easy = new PIXI.Text(__("easy"), {align: 'center', fontSize: 35});
			this.medium = new PIXI.Text(__("medium"), {align: 'center', fontSize: 35});
			this.hard = new PIXI.Text(__("hard"), {align: 'center', fontSize: 35});

			this.switch = new PIXI.Text(">", {fontSize: 40});
			this.switch.position.x = -200;
			this.addChild(this.switch);

			this.difficulty.anchor.set(0.5, 0.5);
			this.easy.anchor.set(0.5, 0.5);
			this.medium.anchor.set(0.5, 0.5);
			this.hard.anchor.set(0.5, 0.5);

			this.addChild(this.difficulty);
			this.addChild(this.easy);
			this.addChild(this.medium);
			this.addChild(this.hard);

			this.difficulty.position.set(0, -height / 4);
			this.easy.position.set(0, -50);
			this.medium.position.set(0, 50);
			this.hard.position.set(0, 150);

			this.enlargeEasy = new anim.ScaleTo(1.4, 1.4).setDuration(100);
			this.minifyEasy = new anim.ScaleTo(1, 1).setDuration(100);
			this.enlargeMedium = new anim.ScaleTo(1.4, 1.4).setDuration(100);
			this.minifyMedium = new anim.ScaleTo(1, 1).setDuration(100);
			this.enlargeHard = new anim.ScaleTo(1.4, 1.4).setDuration(100);
			this.minifyHard = new anim.ScaleTo(1, 1).setDuration(100);

			this.easy.interactive = true;
			this.medium.interactive = true;
			this.hard.interactive = true;

			this.easy.on("mouseover", () => {
				this.minifyEasy.stop();
				anim.scheduler.run(this.enlargeEasy, this.easy);
			});
			this.easy.on("mouseout", () => {
				this.enlargeEasy.stop();
				anim.scheduler.run(this.minifyEasy, this.easy);
			});

			this.medium.on("mouseover", () => {
				this.minifyMedium.stop();
				anim.scheduler.run(this.enlargeMedium, this.medium);
			});
			this.medium.on("mouseout", () => {
				this.enlargeMedium.stop();
				anim.scheduler.run(this.minifyMedium, this.medium);
			});

			this.hard.on("mouseover", () => {
				this.minifyHard.stop();
				anim.scheduler.run(this.enlargeHard, this.hard);
			});
			this.hard.on("mouseout", () => {
				this.enlargeHard.stop();
				anim.scheduler.run(this.minifyHard, this.hard);
			});

			this.displayDifficulty();

			this.easy.on("click", () => { initParams.api.changeSettings(GameDifficulty.easy); this.displayDifficulty(); });
			this.medium.on("click", () => { initParams.api.changeSettings(GameDifficulty.medium); this.displayDifficulty(); });
			this.hard.on("click", () => { initParams.api.changeSettings(GameDifficulty.hard); this.displayDifficulty(); });
		}

		public displayDifficulty() {
			switch (initParams.api.settings.difficulty) {
				case GameDifficulty.easy: this.switch.position.y = -70; break;
				case GameDifficulty.medium: this.switch.position.y = 30; break;
				default: this.switch.position.y = 130;
			}
		}
	}

	export enum GameDifficulty
	{
		easy = 0,
		medium,
		hard
	}
}
