/// <reference path="../../../libs/anim/anim.d.ts"/>
module hello {
	// itt kezdődik a játékspecifikus rész. A többi fájl csak nagyon minimális módosítást igényel. Ennek a tartalmát viszont törölheted már.
	export class Map extends PIXI.Container {
		public goodClicks: number = 0;
		public wrongClicks: number = 0;
		public allClicks: number = 0;

		private goodButton: PIXI.Graphics;
		private wrongButton: PIXI.Graphics;
		private label: PIXI.Text;

		constructor() {
			super();

			this.width = width;
			this.height = height;

			this.goodButton = new PIXI.Graphics();
			this.goodButton.beginFill(0x00a300);
			this.goodButton.drawCircle(width / 3, height / 2, 100);
			this.goodButton.endFill();
			this.addChild(this.goodButton);
			this.goodButton.on("click", this.onGoodButtonClick, this);
			this.goodButton.interactive = true;

			this.wrongButton = new PIXI.Graphics();
			this.wrongButton.beginFill(0xa30000);
			this.wrongButton.drawCircle(width / 3 * 2, height / 2, 100);
			this.wrongButton.endFill();
			this.addChild(this.wrongButton);
			this.wrongButton.on("click", this.onWrongButtonClick, this);
			this.wrongButton.interactive = true;

			this.label = new PIXI.Text(__("clicks left:") + (10 - this.allClicks)); // szöveget mindig a __() metódussal irass ki, az le fogja fordítani mindig a megfelelő nyelvre a szöveget
			this.label.position.x = width / 2;
			this.label.position.y = height / 3;
			this.label.anchor.x = 0.5;
			this.label.style.fontSize = 40;
			this.addChild(this.label);
		}

		private onGoodButtonClick() {
			this.goodClicks++;
			this.allClicks++;
			this.label.text = __("clicks left:") + (10 - this.allClicks);
			if (this.allClicks >= 10) game.showResults(); // ez a hívás fejezi be a játékot
		}

		private onWrongButtonClick() {
			this.wrongClicks++;
			this.allClicks++;
			this.label.text = __("clicks left:") + (10 - this.allClicks);
			if (this.allClicks >= 10) game.showResults(); // ez a hívás fejezi be a játékot
		}
	}
}

