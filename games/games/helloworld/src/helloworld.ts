/// <reference path="../../../libs/scoreCalculator/src/scoreCalculator.d.ts"/>

module hello {
	export class Blocks {
		public app: PIXI.Application;
		public map: Map;

		constructor(container: HTMLElement)
		{
			// a konstruktorban inicializálok mindent amit csak egyszer kell a játék során inicializálni (a játék akár többszöri elinditása során is csak egyszer)
			this.app = new PIXI.Application({
				width: width,
				height: height,
				antialias: true,
				transparent: false,
				resolution: 1,
				backgroundColor: 0xffffff
			});

			container.appendChild(this.app.view);
			window.addEventListener("resize", this.onResize);
			this.onResize();
			anim.scheduler = new anim.Scheduler();
			this.app.stage.addChild(views.empty);
		}

		public newGame() {  // a play gomb callbackje
			this.app.stage.removeChildren();

			log = initParams.api.gameLog;
			log.gameVersion = version;

			this.map = new Map();
			this.app.stage.addChild(this.map);
			this.map.interactiveChildren = true;
		}

		public pause() { // pause gomb callbackje
			this.app.stage.removeChildren();
			this.app.stage.addChild(views.empty);
			this.app.stage.interactive = false;
			this.app.stage.interactiveChildren = false;
			anim.scheduler.pause();
		}

		public resume() { // resume gomb callbackje
			this.app.stage.removeChildren();
			this.app.stage.addChild(this.map);
			this.app.stage.interactive = true;
			this.app.stage.interactiveChildren = true;
			anim.scheduler.resume();
		}

		public stop() { // stop gomb callbackje
			this.app.stage.removeChildren();
			anim.scheduler.stop();
			this.app.stage.addChild(views.empty);
		}

		public exit() { // exit/close gomb callbackje
			if (this.app) {
				this.app.stage.removeChildren();
				anim.scheduler.stop();
				window.removeEventListener("resize", this.onResize);
			}
		}

		private onResize = () => // ez a metódus méretezi át a canvas elemet, hogy kitöltse a képernyőt a játék az általad meghatározott width/height aránnyal (ehhez a metódushoz nem kell nyúlnod)
		{
			const container = this.app.view.parentElement;
			let screenWidth = container.clientWidth;
			let screenHeight = container.clientHeight;
			let tempWidth = width;
			let tempHeight = height;
			const ratio = width / height;

			if (screenWidth > tempWidth) {
				tempWidth = screenWidth;
				tempHeight = screenWidth / ratio;
			}
			if (screenHeight > tempHeight) {
				tempHeight = screenHeight;
				tempWidth = screenHeight * ratio;
			}
			if (tempWidth > screenWidth) {
				tempWidth = screenWidth;
				tempHeight = screenWidth / ratio;
			}
			if (tempHeight > screenHeight) {
				tempHeight = screenHeight;
				tempWidth = screenHeight * ratio;
			}

			this.app.renderer.view.style.width = tempWidth + "px";
			this.app.renderer.view.style.height = tempHeight + "px";
		};

		public getSettings(): MapSettings {
			const result: MapSettings = {};
			switch (initParams.api.settings.difficulty) {
				case 0:
					// TODO do stuff for easy mode (e.g.: result.nunOfObstacles = 1;)
					break;
				case 1:
					// TODO do stuff for medium mode (e.g.: result.nunOfObstacles = 3;)
					break;
				case 2:
					// TODO do stuff for hard mode (e.g.: result.nunOfObstacles = 8;)
					break;
			}
			return result;
		}

		public showSettings(): void { // settigns gomb callbackje
			this.app.stage.removeChildren();
			this.app.stage.interactive = true;
			this.app.stage.interactiveChildren = true;
			this.app.stage.addChild(views.settings);
		}

		public showResults() { // ha a játék véget ér ezt a metódust kell meghívni, és ez kirajzolja a játék végi popupot
			this.app.stage.addChild(views.result);
			anim.scheduler.stop();
			this.map.interactiveChildren = false;

			log.stat = {
				goodClicks : this.map.goodClicks,
				badClicks : this.map.wrongClicks
			};

			const scores = this.calculateScore();
			views.result.setScore(scores.displayed);
			log.displayedScore = scores.displayed;
			log.innerScore = scores.real;
			initParams.onStop(views.result.messageCallback);
		}

		showHelp(): void { // a help gomb callbackje
			this.app.stage.removeChildren();
			this.app.stage.addChild(views.help);
		}

		private calculateScore() { // itt kell a config.json tartalmából inicializálni a scoreCalculatort, ami kiköp magából két pontszámot (1 amit megjelenítünk a usernek, és 1 másik amit nem)
			const diff = initParams.api.settings.difficulty;
			const goodValues = [];
			const values = [];
			const acceptableValues = [];
			const weights = [];
			//number of wrong clicks
			goodValues.push(config[diff].wrongButtonPush.good);
			values.push(log.stat.badClicks);
			acceptableValues.push(config[diff].wrongButtonPush.acceptable);
			weights.push(config[diff].wrongButtonPush.weight);
			//playTime
			goodValues.push(config[diff].playTime.good);
			values.push(initParams.api.timer.duration / 1000);
			acceptableValues.push(config[diff].playTime.acceptable);
			weights.push(config[diff].playTime.weight);

			const calculator = new ScoreCalculator(2, diff);
			return {
				displayed: calculator.getDisplayedScore(goodValues,values,acceptableValues,weights),
				real:      calculator.getRealScore(goodValues,values,acceptableValues,weights),
			}
		};
	}

	export interface MapSettings
	{
		// TODO add interface parts
		// interface a settings visszatérési értékéhez (itt most üres mert a helloworld nem használja a settingset)
	}
}
