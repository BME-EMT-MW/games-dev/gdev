
module hello {
	export class Help extends PIXI.Container {
		private title: PIXI.Text;
		private texts: PIXI.Text[];
		private background: PIXI.Sprite;


		constructor() {
			super();
			this.background = new PIXI.Sprite(PIXI.loader.resources[res.bgForEmpty].texture);
			this.addChild(this.background);
			this.background.anchor.set(0.5, 0.5);
			const size = (width < height) ? width : height;
			this.background.width = size;
			this.background.height = size;
			this.background.alpha = 0.8;
			this.position.set(width / 2, height / 2);

			this.title = new PIXI.Text(__("Help"), {align: 'center', fontSize: 60});
			this.title.anchor.set(0.5, 0.5);
			this.title.position.set(0, -height / 2.5);
			this.addChild(this.title);

			const textStyle = {
				fontSize:30,
				breakWords: true,
				wordWrap: true,
				wordWrapWidth: this.width - 10
			}
			this.texts = [];
			this.texts.push(new PIXI.Text(__("help szöveg 1"),textStyle));
			this.texts.push(new PIXI.Text(__("help szöveg 2"),textStyle));
			this.texts.push(new PIXI.Text(__("help szöveg 3"),textStyle));



			for (let i = 0; i < this.texts.length; i++) {
				this.addChild(this.texts[i]);
				this.texts[i].anchor.set(0, 0.5);
				this.texts[i].position.set(-width / 2 + 10, (i - this.texts.length / 2) * 130 + 100);
			}

		}


	}
}
