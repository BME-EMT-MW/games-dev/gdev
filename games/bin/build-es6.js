const fse = require("fs-extra");
const manifest = require('../package.json');
const parseArgs = require("command-line-args");
const showUsage = require("command-line-usage");
const child_process = require("child_process");
const options = [
    {name: 'help', alias: 'h', type: Boolean, description: 'Print this help message.'},
    {name: 'games', alias: 'g', type: String, multiple: true, description: 'Only build specified games.'},
    {name: 'frame', alias: 'f', type: Boolean, description: 'Only build game frame.'},
    {name: 'target', alias: 't', type: String, defaultValue: "build", description: 'Output directory. (Default is "build")'},
    {name: 'version', alias: 'v', type: String, defaultValue: "", description: 'Version suffix.'}
];
const args = parseArgs(options);

if (args.help) {
    console.log(showUsage([
        {header: 'Options', optionList: options}
    ]));
    return;
}

if (args.frame) {
    buildFrame();
    return;
}

if (args.games) {
    args.games.forEach(buildGame);
    return;
}

generateVersion();
buildFrame();
fse.copySync(`games/games.json`, `${args.target}/_games.json`);
fse.copySync(`i18n`, `${args.target}/_i18n`);
fse.readdirSync("games").filter(isGameDir).forEach(buildGame);

function generateVersion() {
    if (args.version) manifest.version += "-" + args.version;
    fse.outputFileSync(`${args.target}/_version.txt`, manifest.version);
}

function buildFrame() {
    console.log("Building frame (copying)");
    fse.copySync(`frame`, `${args.target}/_frame`);
}

function isGameDir(gameName) {
    return fse.lstatSync("games/" + gameName).isDirectory();
}

function buildGame(gameName) {
    console.log("Building game: " + gameName);

    var cmd = "node_modules/.bin/tsc"
        + " --outFile " + `${args.target}/${gameName}/src/${gameName}.js`
        + " --target ES6"
        + " --sourceRoot './'"
        + " --sourceMap --types --experimentalDecorators -d"
        + ` games/${gameName}/src/config.ts`
    ;
    try {
        child_process.execSync(cmd, {stdio: [0, 1, 2]});
    } catch (e) {
        console.warn("compile error: " + e);
    }

    fse.copySync(`games/${gameName}/index.json`, `${args.target}/${gameName}/index.json`);
    fse.copySync(`games/${gameName}/config.json`, `${args.target}/${gameName}/config.json`);
    fse.copySync(`games/${gameName}/libs`, `${args.target}/${gameName}/libs`);
    fse.copySync(`games/${gameName}/res`, `${args.target}/${gameName}/res`);
    fse.copySync(`games/${gameName}/src`, `${args.target}/${gameName}/src`);

}
